# Example Calculator Package

This is a simple package in order to make some test on packaging principles in Python

For simple codes, use of PYTHONPATH allow to manipulate visibility or not of package in development . But package system manipulation is necessary to share package worlwild.
In the case of development process, principal idea is to separate code to diffuse and code to not diffuse which is not so simple or logical with exclusive use of PytonPath.
So test with packaging system during development process is a good idea, to test code in future sitation of external deployement.

We can see here the configuration :

``` python
from setuptools import setup

setup(
    name='TestSimpleCalculator',
    version='0.0.1',
    author="Fabrice Jumel",
    packages=['calculator'],
    description="TestSimpleCalculator is a simple package sav 2 \
    in order to make some test on packaging principles in Python",
    license='GNU GPLv3',
    python_requires ='>=3.4',
)
```



# Some commands to  install and desinstall a package

## Directly from sources
### Hard copy of the package in the default package location 
For example, 
if call with root user : /usr/local/lib/python3.6/dist-packages/
if call with normal user : ~/.local/lib/python3.6/dist-packages/

``` console
# from directory containing setup.py
pip install .
```

You can check installation with 

``` console
tp@tp-VM:~$ pip freeze | grep TestSimpleCalculator
TestSimpleCalculator==0.0.1

```
### Desinstalation of a package
desinstaltion is simple, always the same process whatever the type of installation

``` console
pip uninstall TestSimpleCalculator
```

more precisley what happend, time to see the default location

``` console
tp@tp-VM:~$ pip uninstall TestSimpleCalculator
Uninstalling TestSimpleCalculator-0.0.1:
  /home/tp/.local/lib/python3.6/site-packages/TestSimpleCalculator-0.0.1.egg-info
  /home/tp/.local/lib/python3.6/site-packages/calculator/__init__.py
  /home/tp/.local/lib/python3.6/site-packages/calculator/__pycache__/__init__.cpython-36.pyc
  /home/tp/.local/lib/python3.6/site-packages/calculator/__pycache__/setup.cpython-36.pyc
  /home/tp/.local/lib/python3.6/site-packages/calculator/__pycache__/simplecalculator.cpython-36.pyc
  /home/tp/.local/lib/python3.6/site-packages/calculator/setup.py
  /home/tp/.local/lib/python3.6/site-packages/calculator/simplecalculator.py
Proceed (y/n)? y
  Successfully uninstalled TestSimpleCalculator-0.0.1
tp@tp-VM:~$  pip freeze | grep TestSimpleCalculator
tp@tp-VM:~$ 
```
### symbolick Link of the package in the default package location 
Usefull to test from different temrinals during code developmment. Not so different from direct use of PYTHONPATH but usefull to limit visibility to specifics package in same directories

``` console
# from directory containing setup.py
pip install -e .
```
## Create a source deployable version of the package
``` console
# from directory containing setup.py
python setup.py  sdist
```

More details of execution in my example
``` console
tp@tp-VM:~/essai/tp1_ex8v0$ tree
.
├── calculator
│   ├── __init__.py
│   ├── setup.py
│   └── simplecalculator.py
├── LICENSE
├── README.md
├── setup.py
└── tests
    ├── __init__.py
    └── mytest.py
tp@tp-VM:~/essai/tp1_ex8v0$ python setup.py sdist
running sdist
running egg_info
creating TestSimpleCalculator.egg-info
writing TestSimpleCalculator.egg-info/PKG-INFO
writing dependency_links to TestSimpleCalculator.egg-info/dependency_links.txt
writing top-level names to TestSimpleCalculator.egg-info/top_level.txt
writing manifest file 'TestSimpleCalculator.egg-info/SOURCES.txt'
reading manifest file 'TestSimpleCalculator.egg-info/SOURCES.txt'
writing manifest file 'TestSimpleCalculator.egg-info/SOURCES.txt'
running check
warning: check: missing required meta-data: url

warning: check: missing meta-data: if 'author' supplied, 'author_email' must be supplied too

creating TestSimpleCalculator-0.0.1
creating TestSimpleCalculator-0.0.1/TestSimpleCalculator.egg-info
creating TestSimpleCalculator-0.0.1/calculator
copying files to TestSimpleCalculator-0.0.1...
copying README.md -> TestSimpleCalculator-0.0.1
copying setup.py -> TestSimpleCalculator-0.0.1
copying TestSimpleCalculator.egg-info/PKG-INFO -> TestSimpleCalculator-0.0.1/TestSimpleCalculator.egg-info
copying TestSimpleCalculator.egg-info/SOURCES.txt -> TestSimpleCalculator-0.0.1/TestSimpleCalculator.egg-info
copying TestSimpleCalculator.egg-info/dependency_links.txt -> TestSimpleCalculator-0.0.1/TestSimpleCalculator.egg-info
copying TestSimpleCalculator.egg-info/top_level.txt -> TestSimpleCalculator-0.0.1/TestSimpleCalculator.egg-info
copying calculator/__init__.py -> TestSimpleCalculator-0.0.1/calculator
copying calculator/setup.py -> TestSimpleCalculator-0.0.1/calculator
copying calculator/simplecalculator.py -> TestSimpleCalculator-0.0.1/calculator
Writing TestSimpleCalculator-0.0.1/setup.cfg
creating dist
Creating tar archive
removing 'TestSimpleCalculator-0.0.1' (and everything under it)
tp@tp-VM:~/essai/tp1_ex8v0$ tree
.
├── calculator
│   ├── __init__.py
│   ├── setup.py
│   └── simplecalculator.py
├── dist
│   └── TestSimpleCalculator-0.0.1.tar.gz
├── LICENSE
├── README.md
├── setup.py
├── tests
│   ├── __init__.py
│   └── mytest.py
└── TestSimpleCalculator.egg-info
    ├── dependency_links.txt
    ├── PKG-INFO
    ├── SOURCES.txt
    └── top_level.txt

    
```


## Install a  deployable version of the package
you can install the different format with equivaent commands

``` console
# from directory the deployable package (than you  can have generate or download)
# for example if generated with sdist option
python pip TestSimpleCalculator-0.0.1.tar.gz
```


## Create diferent  deployable version of the package
Some examples, we will not explain differencies here.

``` console
python setup.py bdist sdist bdist_wheel
```

``` console
python setup.py bdist sdist bdist_wheel
tp@tp-VM:~/essai/tp1_ex8v0$ tree dist
dist
├── TestSimpleCalculator-0.0.1.linux-x86_64.tar.gz
├── TestSimpleCalculator-0.0.1-py3-none-any.whl
└── TestSimpleCalculator-0.0.1.tar.gz
```
Example of installation , you just need the .whl that you could have download in order to install 
``` console
tp@tp-VM:~/essai/tp1_ex8v0/dist$ pip install TestSimpleCalculator-0.0.1-py3-none-any.whl 
Processing ./TestSimpleCalculator-0.0.1-py3-none-any.whl
Installing collected packages: TestSimpleCalculator
Successfully installed TestSimpleCalculator-0.0.1
tp@tp-VM:~/essai/tp1_ex8v0/dist$ 
```


## About Direct use of setup.py for installation of package

| :exclamation:  The direct use of the setup.py script is possible but the use is deprecetated and reserved for expert users   |
|-----------------------------------------------------------------------------------------------------------------------------|



Normal root installation : hard copy of the package from initial location  to default root package localisation (for example)  /usr/local/lib/python3.6/dist-packages/ need to be root, not a good idea in general.

``` console
python setup.py install 
```

Normal user installation : hard copy of the package from initial location  to default user package localisation (for example)  ~/.local/lib/python3.6/dist-packages/

``` console
python setup.py install --user
```

Normal user installation : hard copy of the package from initial location  to default user package localisation (for example)  ~/.local/lib/python3.6/dist-packages/

``` console
python setup.py install --user
```

Make a symbolic link  in root  package location (for example)  /usr/local/lib/python3.6/dist-packages/ need to be root, not a good idea in general

``` console
python setup.py develop
```

Make a symbolic link in user package location (for example  ~/.local/lib/python3.6/dist-packages/ )

``` console
python setup.py develop --user
```


